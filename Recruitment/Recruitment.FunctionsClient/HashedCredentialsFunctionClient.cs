﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Recruitment.Contracts;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Recruitment.FunctionsClient
{
    public class HashedCredentialsFunctionClient : IHashedCredentialsFunctionClient
    {
        private const string AZURE_FUNCTION_PATH = "AzureFunctionsPath";
        private const string CALCULATE_HASH_FUNCTION = "CalculateHash";

        private readonly string _functionPath;
        private readonly IHttpClientFactory _httpClientFactory;

        public HashedCredentialsFunctionClient(IConfiguration configuration, IHttpClientFactory httpClientFactory)
        {
            _functionPath = configuration[AZURE_FUNCTION_PATH];
            _httpClientFactory = httpClientFactory;
        }

        public async Task<HashedCredentials> GetHashedCredentials(Credentials credentials)
        {
            var client = _httpClientFactory.CreateClient();

            var response = await client.PostAsync(Path.Combine(_functionPath, CALCULATE_HASH_FUNCTION), new StringContent(JsonConvert.SerializeObject(credentials)));

            response.EnsureSuccessStatusCode();

            try
            {
                return JsonConvert.DeserializeObject<HashedCredentials>(await response.Content.ReadAsStringAsync());
            }
            catch(Exception exception)
            {
                Console.WriteLine(exception);
            }

            return null;
        }
    }
}
