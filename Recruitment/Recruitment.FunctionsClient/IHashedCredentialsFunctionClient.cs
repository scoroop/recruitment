﻿using Recruitment.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.FunctionsClient
{
    public interface IHashedCredentialsFunctionClient
    {
        Task<HashedCredentials> GetHashedCredentials(Credentials credentials);
    }
}
