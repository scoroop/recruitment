﻿using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Recruitment.Contracts;
using Recruitment.Functions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Functions.Tests
{
    [TestClass()]
    public class CalculateHashFunctionTests
    {

        public static HttpRequest CreateHttpRequest()
        {
            var context = new DefaultHttpContext();
            var request = context.Request;
            
            return request;
        }

        [TestMethod()]
        public async Task CalculateHashFunction_Test()
        {
            //Arrange
            var hasher = Substitute.For<IHasher>();
            hasher.GetHash(Arg.Any<string>()).Returns("XYZ");
            var calculateFunction = new CalculateHashFunction(hasher);

            var logger = Substitute.For<ILogger>();

            //Act
            var hashedValue = await calculateFunction.Run(CreateHttpRequest(), logger);

            //Assert
            hashedValue.Should().BeOfType(typeof(OkObjectResult));
            (hashedValue as OkObjectResult).Value.Should().BeAssignableTo(typeof(HashedCredentials));
            ((hashedValue as OkObjectResult).Value as HashedCredentials).HashValue.Should().Be("XYZ");
        }
    }
}