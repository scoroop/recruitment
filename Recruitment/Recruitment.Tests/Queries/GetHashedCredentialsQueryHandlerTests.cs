﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Recruitment.API.Queries;
using Recruitment.Contracts;
using Recruitment.FunctionsClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Recruitment.API.Queries.Tests
{
    [TestClass()]
    public class GetHashedCredentialsQueryHandlerTests
    {
        [TestMethod()]
        public async Task GetHAshedCredentialsQueryHandler_Handle_ShouldReturnValue()
        {
            //This is just an example of test on CQRS query level and it tests business, not only implementational details so it could be sensible in real life scenario
            //In this particular situation it is a kind of duplicate but instead of mocking the whole CQRS query calling from api controller we can mock only an azure function client 

            //Arrange
            var azureFunctionClientMock = Substitute.For<IHashedCredentialsFunctionClient>();
            azureFunctionClientMock.GetHashedCredentials(Arg.Is<Credentials>(x => x.Login.StartsWith("Test"))).Returns(Task.FromResult(new HashedCredentials() { HashValue = "AAAA" }));
            azureFunctionClientMock.GetHashedCredentials(Arg.Is<Credentials>(x => x.Login.StartsWith("AnotherGuy"))).Returns(Task.FromResult(new HashedCredentials() { HashValue = "BBBB" }));
            var query1 = new GetHashedCredentialsQuery() { Credentials = new Credentials() { Login = "Test123456", Password = "Test" } };
            var query2 = new GetHashedCredentialsQuery() { Credentials = new Credentials() { Login = "AnotherGuy", Password = "Test" } };
            var queryHandler = new GetHashedCredentialsQueryHandler(azureFunctionClientMock);

            //Act
            //In more complex situations could be put into separate tests - I just wanted to show we can create various scenarios in the same environment when mocking
            var result1 = await queryHandler.Handle(query1, default(CancellationToken));
            var result2 = await queryHandler.Handle(query2, default(CancellationToken));

            //Assert
            result1.HashValue.Should().Be("AAAA");
            result2.HashValue.Should().Be("BBBB");
        }
    }
}