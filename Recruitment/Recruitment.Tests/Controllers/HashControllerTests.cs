﻿using FluentAssertions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Recruitment.API.Controllers;
using Recruitment.Contracts;
using Recruitment.FunctionsClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.API.Controllers.Tests
{
    [TestClass()]
    public class HashControllerTests
    {
        [TestMethod()]
        public async Task GetHashedCredentials_ShouldReturnHashedValue_Test()
        {
            //Arrange
            var mediator = Substitute.For<IMediator>();
            mediator.Send<HashedCredentials>(Arg.Any<IRequest<HashedCredentials>>()).Returns(Task.FromResult(new HashedCredentials() { HashValue = "ABC" }));
            var hashController = new HashController(mediator);

            //Act
            var result = await hashController.GetHashedCredentials(new Credentials() { Login = "Test", Password = "Test" });

            //Assert
            result.Should().BeOfType(typeof(OkObjectResult));
            (result as OkObjectResult).Value.Should().BeAssignableTo(typeof(HashedCredentials));
            ((result as OkObjectResult).Value as HashedCredentials).HashValue.Should().Be("ABC");
        }

        [TestMethod()]
        public async Task GetHashedCredentials_ShouldThrowExceptionWhenLoginIsNull_Test()
        {
            //Arrange
            var mediator = Substitute.For<IMediator>();
            mediator.Send<HashedCredentials>(Arg.Any<IRequest<HashedCredentials>>()).Returns(Task.FromResult(new HashedCredentials() { HashValue = "ABC" }));
            var hashController = new HashController(mediator);

            //Act
            var result = await hashController.GetHashedCredentials(new Credentials() { Login = null, Password = "Test" });

            //Assert
            result.Should().BeOfType(typeof(BadRequestResult));
        }

        [TestMethod()]
        public async Task GetHashedCredentials_ShouldThrowExceptionWhenPasswordIsNull_Test()
        {
            //Arrange
            var mediator = Substitute.For<IMediator>();
            mediator.Send<HashedCredentials>(Arg.Any<IRequest<HashedCredentials>>()).Returns(Task.FromResult(new HashedCredentials() { HashValue = "ABC" }));
            var hashController = new HashController(mediator);

            //Act
            var result = await hashController.GetHashedCredentials(new Credentials() { Login = "Test", Password = null });

            //Assert
            result.Should().BeOfType(typeof(BadRequestResult));
        }
    }
}