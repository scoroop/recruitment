﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Functions
{
    public interface IHasher
    {
        string GetHash(string rawString);
    }
}
