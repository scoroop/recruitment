using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Recruitment.Contracts;

namespace Recruitment.Functions
{
    public class CalculateHashFunction
    {
        private readonly IHasher _hasher;

        public CalculateHashFunction(IHasher hasher)
        {
            _hasher = hasher;
        }

        [FunctionName("CalculateHash")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)] HttpRequest request,
            ILogger log)
        {
            log.LogInformation("Calculate hash function triggered");

            string requestBody = await new StreamReader(request.Body).ReadToEndAsync();

            //we could deserialize it, but since making hash out of both login and password probably doesn't have too much of a use I thought it makes sense to just hash json value directly
            //var credentials = JsonConvert.DeserializeObject<Credentials>(requestBody);

            var hashedValue = _hasher.GetHash(requestBody);

            return new OkObjectResult(new HashedCredentials() { HashValue = hashedValue });
        }
    }
}
