﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Recruitment.Functions
{
    ///Due to collision problems with MD5, MS suggest to use SHA256 instead - therefore I decoupled azure function from algorithm by introducting IHasher abstraction
    public class MD5Hasher : IHasher
    {
        public string GetHash(string rawString)
        {
            using (var md5 = MD5.Create())
            {
                var inputBytes = System.Text.Encoding.ASCII.GetBytes(rawString);
                var hashBytes = md5.ComputeHash(inputBytes);

                var sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
    }
}
