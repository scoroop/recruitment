﻿using Microsoft.Extensions.DependencyInjection;
using Recruitment.FunctionsClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Recruitment.API
{
    public static class DependencyInjectionRegistrations
    {
        public static IServiceCollection RegisterDependencies(this IServiceCollection services)
        {
            services.AddScoped<IHashedCredentialsFunctionClient, HashedCredentialsFunctionClient>();

            return services;
        }

    }
}
