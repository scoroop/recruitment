﻿using MediatR;
using Recruitment.Contracts;
using Recruitment.FunctionsClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Recruitment.API.Queries
{
    /// <summary>
    /// This layer is not necessary and might be perceived as overkill in such a small solution. What I wanted to achieve was to show that it is worth it to start with CQRS from the very beginning 
    /// </summary>
    public class GetHashedCredentialsQuery : IRequest<HashedCredentials>
    {
        public Credentials Credentials { get; set; }
    }

    public class GetHashedCredentialsQueryHandler : IRequestHandler<GetHashedCredentialsQuery, HashedCredentials>
    {
        private readonly IHashedCredentialsFunctionClient _functionClient;

        public GetHashedCredentialsQueryHandler(IHashedCredentialsFunctionClient functionClient)
        {
            _functionClient = functionClient;
        }

        public async Task<HashedCredentials> Handle(GetHashedCredentialsQuery request, CancellationToken cancellationToken)
        {
            return await _functionClient.GetHashedCredentials(request.Credentials);
        }
    }
}
