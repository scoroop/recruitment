﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Recruitment.API.Queries;
using Recruitment.Contracts;
using Recruitment.FunctionsClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.API.Controllers
{
    [ApiController]
    [Route("api/hash")]
    public class HashController : ControllerBase
    {
        private readonly ILogger<HashController> _logger;
        private readonly IMediator _mediator;

        public HashController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> GetHashedCredentials(Credentials credentials)
        {
            if (credentials.Login == null)
            {
                return BadRequest();
            }
            if (credentials.Password == null)
            {
                return BadRequest();
            }

            var response = await _mediator.Send<HashedCredentials>(new GetHashedCredentialsQuery()
            {
                Credentials = credentials
            });

            return Ok(response);
        }
    }
}
